
-- SUMMARY --

*Most Useful for Multi site purposes*
This module is for displaying a splash page to users if they have a option
available to visit their country's website in a very cool and configurable
pop-up.
It also supports different type of animations plus what text you want to see.

It also has a configuration page to add new website urls and flush them in case
url is added by mistake.

Usage example, If a United State's visitor visits a Germany website and if
United State's website is available which user might not be having knowledge of,
visitor will get a pop up that you can visit your country's website or else they
can stay on the website itself.


-- REQUIREMENTS --

Download the library from https://github.com/lhuria94/sweetalert/archive/master.
zip


How to configure:

1) To add: Navigate to admin/config/ip-locator/create
2) Splash message settings: Navigate to admin/config/ip-locator/splash-message
-settings
3) To update added urls or list out: Navigate to admin/config/ip-locator/ip-
locator-configuration
4) That's it!



-- CONTACT --

Current maintainers:
* Love huria (lhuria94) - https://www.drupal.org/user/3229001/
