/**
 * @file
 * This file is used for calling sweetalert js snippet.
 */

(function ($) {
  'use strict';
  // This function is strict.
  Drupal.behaviors.ip_locator_with_splash = {
    attach: function () {
      var result = Drupal.settings.ip_locator_with_splash.result;
      var animation_type = Drupal.settings.ip_locator_with_splash.animation_type;
      var show_loader = Drupal.settings.ip_locator_with_splash.show_loader;
      var show_continue = Drupal.settings.ip_locator_with_splash.show_continue;
      var title = Drupal.settings.ip_locator_with_splash.title;
      var description_before_text = Drupal.settings.ip_locator_with_splash.description_before_text;
      var description_after_text = Drupal.settings.ip_locator_with_splash.description_after_text;
      var continue_site = Drupal.settings.ip_locator_with_splash.continue_site;
      var redirect_before_text = Drupal.settings.ip_locator_with_splash.redirect_before_text;
      var redirect_after_text = Drupal.settings.ip_locator_with_splash.redirect_after_text;
      var country_name = Drupal.settings.ip_locator_with_splash.country_name;
      var alerted = sessionStorage.getItem('alerted') || '';
      if (alerted !== 'yes') {
        swal({
          title: title + ' ' + country_name,
          text: description_before_text + ' ' + country_name + ' ' + description_after_text,
          showCancelButton: show_continue,
          closeOnConfirm: false,
          animation: animation_type,
          confirmButtonText: redirect_before_text + ' ' + country_name + ' ' + redirect_after_text,
          cancelButtonText: continue_site,
          showLoaderOnConfirm: show_loader
        },
        function (inputValue) {
          if (inputValue === true) {
            swal('Redirecting..');
            window.location = result;
          }
        });
        sessionStorage.setItem('alerted', 'yes');
      }
    }
  };
})(jQuery);
