<?php

/**
 * @file
 * This file is for all admin related configuration forms.
 */

/**
 * Implements hook_admin_setting_form().
 *
 * This function provides a form to admin interface.
 */
function ip_locator_with_splash_admin_setting_form() {
  $form['create_link'] = array(
    '#type' => 'link',
    '#title' => t('Add new website url'),
    '#href' => 'admin/config/system/ip-locator-configuration/create',
    '#attributes' => array('id' => 'ip-locator-create-link'),
  );
  // Count result.
  $count_nodes = db_query('SELECT id FROM {country_url_data}')->rowCount();
  if ($count_nodes > 0) {
    $form['ip_locator'] = array(
      '#type' => 'fieldset',
      '#title' => t('IP Locator Configuration'),
      '#description' => t('Allocate websites url according to their country code.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    // Get all records.
    $sql = "Select * from {country_url_data}";
    $result = db_query($sql);
    foreach ($result as $val) {
      $form['ip_locator'][$val->country_code] = array(
        '#title' => check_plain($val->country_label),
        '#type' => 'textfield',
        '#default_value' => $val->website_url,
        '#required' => TRUE,
      );
      $id = $val->id;
      $form['ip_locator']['delete-' . $val->country_code] = array(
        '#type' => 'link',
        '#title' => t('Delete'),
        '#href' => 'admin/config/system/ip-locator-configuration/' . $id . '/delete',
        '#attributes' => array('id' => 'ip-locator-delete-link'),
      );
    }
  }
  else {
    $form['ip_locator_help'] = array(
      '#markup' => '<div class="ip-locator-help">' . t('No result found.') . '</div>',
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Update'));
  return $form;
}

/**
 * Submit handler for ip locator config form.
 */
function ip_locator_with_splash_admin_setting_form_submit($form, &$form_state) {
  // Update config form values.
  try {
    foreach ($form_state['values'] as $key => $value) {
      // Table name no longer needs {}.
      db_update('country_url_data')->fields(array(
        'website_url' => $value,
        'created' => REQUEST_TIME,
      ))->condition('country_code', $key, '=')->execute();
    }
    drupal_set_message(t('Your website url has been updated successfully.'));
  }
  catch (PDOException$e) {
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }
}

/**
 * Remove confirm handler for ip locator config form.
 */
function ip_locator_with_splash_admin_setting_form_delete($form, &$form_state, $arg) {
  // Confirm delete form.
  $result = db_query("SELECT COUNT(*) FROM {country_url_data} WHERE id = :id", array(':id' => $arg))->fetchField();
  if ($result != 0) {
    $form = array();
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $arg,
    );
    return confirm_form($form, t('Are you sure you want to delete this item?'), '/admin/config/system/ip-locator-configuration/', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
  }
  else {
    drupal_set_message(t('Data does not exists in the system.'), 'error');
  }
  return $form;
}

/**
 * Submit handler for ip locator config delete form.
 */
function ip_locator_with_splash_admin_setting_form_delete_submit($form, &$form_state) {
  try {
    $id = $form_state['values']['id'];
    // Table name no longer needs {}.
    db_delete('country_url_data')
      ->condition('id', $id, '=')->execute();
    drupal_set_message(t('Your website url has been deleted successfully.'));
    $form_state['redirect'] = 'admin/config/system/ip-locator-configuration';
  }
  catch (PDOException$e) {
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }
}

/**
 * Implements hook_validate().
 */
function ip_locator_with_splash_admin_setting_form_validate($form, &$form_state) {
  $ccode = ip_locator_with_splash_country_codes();
  foreach ($form_state['values'] as $key => $value) {
    if (array_key_exists($key, $ccode)) {
      if (!filter_var($value, FILTER_VALIDATE_URL)) {
        form_set_error($key, t('Please use correct url.'));
      }
    }
  }
  return TRUE;
}

/**
 * Implements hook_form().
 *
 * This function provides a confirm action.
 */
function ip_locator_with_splash_flush_form($form, $form_state) {
  $count_nodes = db_query('SELECT id FROM {country_url_data}')->rowCount();
  if ($count_nodes > 0) {
    return confirm_form($form, t('Are you sure want to flush all urls?'), '/admin/config/system/ip-locator-configuration');
  }
  else {
    drupal_set_message(t('No urls exists in the table to flush.'), 'warning');
    $form_state['redirect'] = 'admin/config/system/ip-locator-configuration';
  }
}

/**
 * Implements hook_form().
 *
 * This function provides a confirm action.
 */
function ip_locator_with_splash_flush_form_submit($form, $form_state) {
  if (db_table_exists('country_url_data')) {
    db_truncate('country_url_data')->execute();
    drupal_set_message(t('Urls flushed.'));
    $form_state['redirect'] = 'admin/config/system/ip-locator-configuration';
  }
}

/**
 * Implements hook_form().
 *
 * This function provides a form to admin interface.
 */
function ip_locator_with_splash_weburl_create_form() {
  // Display Configuraion.
  $form['ip_locator_create'] = array(
    '#type' => 'fieldset',
    '#title' => t('Website URLs configuration'),
    '#description' => t('Add new website urls according to country codes.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $option = ip_locator_with_splash_country_codes();
  $form['ip_locator_create']['ip_locator_with_splash_country_label'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ip_locator_with_splash_country_label', ''),
    '#required' => TRUE,
  );
  $form['ip_locator_create']['ip_locator_with_splash_country_code'] = array(
    '#title' => t('Country'),
    '#type' => 'select',
    '#options' => $option,
    '#default_value' => variable_get('ip_locator_with_splash_country_code', ''),
  );
  $form['ip_locator_create']['ip_locator_with_splash_website_url'] = array(
    '#title' => t('Website URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ip_locator_with_splash_website_url', ''),
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => 'Save');
  return $form;
}

/**
 * Submit handler for weburl create form.
 */
function ip_locator_with_splash_weburl_create_form_submit($form, &$form_state) {
  try {
    // Save website url entry.
    db_insert('country_url_data')->fields(array(
      'country_label' => $form_state['values']['ip_locator_with_splash_country_label'],
      'country_code' => $form_state['values']['ip_locator_with_splash_country_code'],
      'website_url' => $form_state['values']['ip_locator_with_splash_website_url'],
    ))->execute();
    drupal_set_message(t('Your website url have been saved.'));
    $form_state['redirect'] = 'admin/config/system/ip-locator-configuration';
  }
  catch (PDOException$e) {
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }
}

/**
 * Implements hook_validate().
 */
function ip_locator_with_splash_weburl_create_form_validate($form, &$form_state) {
  $url = $form_state['values']['ip_locator_with_splash_website_url'];
  $code = $form_state['values']['ip_locator_with_splash_country_code'];

  $result = db_query("SELECT COUNT(*) FROM {country_url_data} WHERE country_code = :country_code", array(':country_code' => $code))->fetchField();
  if (!filter_var($url, FILTER_VALIDATE_URL)) {
    form_set_error('ip_locator_with_splash_website_url', t('Please use correct url.'));
  }
  elseif ($result != 0) {
    form_set_error('ip_locator_with_splash_country_code', t('This country code has already been added to the system.'));
  }
  else {
    return TRUE;
  }
}

/**
 * Implements hook_form().
 *
 * This function provides a form to admin interface.
 */
function ip_locator_with_splash_splash_setting_form() {
  // Display Configuraion.
  $form['ip_locator_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Splash display configuration'),
    '#description' => t('Configure how the splash message will be displayed.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $option_at = array(
    'pop' => t('pop'),
    'slide-from-top' => t('slide-from-top'),
    'slide-from-bottom' => t('slide-from-bottom'),
  );
  $form['ip_locator_display']['ip_locator_with_splash_animation_type'] = array(
    '#title' => t('Animation'),
    '#type' => 'select',
    '#options' => $option_at,
    '#default_value' => variable_get('ip_locator_with_splash_animation_type', 'slide-from-top'),
  );
  $option_sl = array(
    'true' => t('Yes'),
    'false' => t('No'),
  );
  $form['ip_locator_display']['ip_locator_with_splash_show_loader'] = array(
    '#title' => t('Show loader while Redirect'),
    '#type' => 'select',
    '#options' => $option_sl,
    '#default_value' => variable_get('ip_locator_with_splash_show_loader', 'true'),
  );
  $option_sc = array(
    'true' => t('Yes'),
    'false' => t('No'),
  );
  $form['ip_locator_display']['ip_locator_with_splash_show_continue'] = array(
    '#title' => t('Show "Continue to site" button'),
    '#type' => 'select',
    '#options' => $option_sc,
    '#default_value' => variable_get('ip_locator_with_splash_show_continue', 'true'),
  );
  // End.
  // Message Configuration.
  $form['ip_locator_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Splash message configuration'),
    '#description' => t('Configure the splash message.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ip_locator_message']['ip_locator_with_splash_title_before_text'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#description' => t('Configure the title text before country name.'),
    '#default_value' => variable_get('ip_locator_with_splash_title_before_text', 'Your current location is '),
  );
  $form['ip_locator_message']['ip_locator_with_splash_description_before_text'] = array(
    '#title' => t('Description (Before Country text)'),
    '#type' => 'textfield',
    '#description' => t('Configure the description text before country name.'),
    '#default_value' => variable_get('ip_locator_with_splash_description_before_text', 'You can also visit our '),
  );
  $form['ip_locator_message']['ip_locator_with_splash_description_after_text'] = array(
    '#title' => t('Description (After Country text)'),
    '#type' => 'textfield',
    '#description' => t('Configure the description text after country name.'),
    '#default_value' => variable_get('ip_locator_with_splash_description_after_text', 'website.'),
  );
  // End.
  // Buttons Configuration.
  $form['ip_locator_buttons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Splash buttons configuration'),
    '#description' => t('Configure the splash button labels.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ip_locator_buttons']['ip_locator_with_splash_continue_site'] = array(
    '#title' => t('Label for "Continue to site" button'),
    '#type' => 'textfield',
    '#description' => t('Configure the "Continue to site" label.'),
    '#default_value' => variable_get('ip_locator_with_splash_continue_site', 'Continue to site'),
  );
  $form['ip_locator_buttons']['ip_locator_with_splash_redirect_before_text'] = array(
    '#title' => t('Redirect Button (Before Country text)'),
    '#type' => 'textfield',
    '#description' => t('Configure the redirect button label before country name.'),
    '#default_value' => variable_get('ip_locator_with_splash_redirect_before_text', 'Visit '),
  );
  $form['ip_locator_buttons']['ip_locator_with_splash_redirect_after_text'] = array(
    '#title' => t('Redirect Button (After Country text)'),
    '#type' => 'textfield',
    '#description' => t('Configure the redirect button label after country name.'),
    '#default_value' => variable_get('ip_locator_with_splash_redirect_after_text', 'website'),
  );
  // End.
  return system_settings_form($form);
}
